# Generated by Django 3.0.5 on 2020-05-20 14:48

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('movie_app', '0018_auto_20200519_1645'),
    ]

    operations = [
        migrations.AlterField(
            model_name='genrenavbarmodel',
            name='genres',
            field=models.CharField(blank=True, max_length=10, null=True, unique=True),
        ),
        migrations.AlterField(
            model_name='languagenavbarmodel',
            name='languages',
            field=models.CharField(blank=True, max_length=10, null=True, unique=True),
        ),
        migrations.AlterField(
            model_name='yearnavbarmodel',
            name='years',
            field=models.PositiveIntegerField(blank=True, null=True, unique=True),
        ),
    ]
