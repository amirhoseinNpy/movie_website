# Generated by Django 3.0.5 on 2020-05-19 12:15

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('movie_app', '0017_auto_20200519_1643'),
    ]

    operations = [
        migrations.AlterField(
            model_name='navbarmodel',
            name='genre_navbar_model',
            field=models.ManyToManyField(to='movie_app.GenreNavbarModel'),
        ),
        migrations.AlterField(
            model_name='navbarmodel',
            name='languages_navbar_model',
            field=models.ManyToManyField(to='movie_app.LanguageNavbarModel'),
        ),
        migrations.AlterField(
            model_name='navbarmodel',
            name='years_navbar_model',
            field=models.ManyToManyField(to='movie_app.YearNavbarModel'),
        ),
    ]
