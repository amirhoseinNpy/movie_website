# Generated by Django 3.0.5 on 2020-05-10 15:34

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('movie_app', '0008_auto_20200508_1542'),
    ]

    operations = [
        migrations.AlterField(
            model_name='moviemodel',
            name='genres',
            field=models.CharField(
                choices=[('action', 'Action'), ('comedy', 'Comedy'), ('drama', 'Drama'), ('romance', 'Romance')],
                max_length=50),
        ),
    ]
