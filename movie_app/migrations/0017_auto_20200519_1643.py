# Generated by Django 3.0.5 on 2020-05-19 12:13

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('movie_app', '0016_auto_20200519_0530'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='navbarmodel',
            name='genre_navbar_model',
        ),
        migrations.AddField(
            model_name='navbarmodel',
            name='genre_navbar_model',
            field=models.ManyToManyField(blank=True, null=True, to='movie_app.GenreNavbarModel'),
        ),
        migrations.RemoveField(
            model_name='navbarmodel',
            name='languages_navbar_model',
        ),
        migrations.AddField(
            model_name='navbarmodel',
            name='languages_navbar_model',
            field=models.ManyToManyField(blank=True, null=True, to='movie_app.LanguageNavbarModel'),
        ),
        migrations.RemoveField(
            model_name='navbarmodel',
            name='years_navbar_model',
        ),
        migrations.AddField(
            model_name='navbarmodel',
            name='years_navbar_model',
            field=models.ManyToManyField(blank=True, null=True, to='movie_app.YearNavbarModel'),
        ),
    ]
