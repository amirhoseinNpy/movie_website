import requests


def client():
    # credentials = {'username': 'usr1', 'password': '1234'}
    #
    # response = requests.post('http://127.0.0.1:8000/account/api/login/',
    #                          data=credentials)

    token_h = 'Token 9655a4b925a9b9dadbc14571e550460f3a33d94c'
    headers = {'Authorization': token_h}

    response = requests.get('http://127.0.0.1:8000/movie/api/movie/',
                             headers=headers)

    print('Status Code: ', response.status_code)
    response_data = response.json()
    print(response_data)


if __name__ == '__main__':
    client()
