from rest_framework import routers
from django.urls import path, include

from . import views

# api
# router = routers.DefaultRouter()
# router.register('movie', views.MovieApiView)
# pre-urls = 'movie/api/'
urlpatterns = [
    # path('', include(router.urls), name='movie'),
    path('movie/', views.MovieListApiView.as_view(), name='movie_list'),
    path('movie/<int:pk>', views.MovieDetailApiView.as_view(), name='movie_detail'),
    path('cast/', views.CastListApiView.as_view(), name='cast_list'),
    path('cast/<int:pk>/', views.CastDetailApiView.as_view(), name='cast_detail'),
]
