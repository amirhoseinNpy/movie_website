from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics, mixins

from movie_app.api.serializers import MovieSerializer, CastSerializer, CastSerializer2
from ..models import MovieModel, CastModel


# class MovieApiView(viewsets.ModelViewSet):
#     queryset = MovieModel.objects.all()
#     serializer_class = MovieSerializer

class MovieListApiView(generics.ListAPIView):
    queryset = MovieModel.objects.all()
    serializer_class = MovieSerializer


class MovieDetailApiView(generics.RetrieveUpdateAPIView):
    queryset = MovieModel.objects.all()
    serializer_class = MovieSerializer


# class CastListApiView(APIView):
#
#     def get(self, request):
#         casts = CastModel.objects.all()
#         data = CastSerializer(casts, many=True).data
#         return Response(data=data)


# class CastDetailApiView(APIView):
#
#     def get(self, request, pk):
#         cast = get_object_or_404(CastModel, pk=pk)
#         data = CastSerializer(cast).data
#         return Response(data=data)


class CastListApiView(generics.ListCreateAPIView):
    queryset = CastModel.objects.all()

    def get_serializer_class(self):
        if self.request.user.is_authenticated:
            return CastSerializer
        return CastSerializer2


class CastDetailApiView(generics.RetrieveUpdateDestroyAPIView):
    queryset = CastModel.objects.all()
    serializer_class = CastSerializer
