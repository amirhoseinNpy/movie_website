from rest_framework import serializers
from movie_app.models import MovieModel, CastModel


class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = MovieModel
        fields = '__all__'
        # fields = ('id', 'name', 'language', 'year', 'published')


class CastSerializer(serializers.ModelSerializer):
    class Meta:
        model = CastModel
        fields = '__all__'
        extra_kwargs = {'name': {'read_only': True}}


class CastSerializer2(serializers.ModelSerializer):
    class Meta:
        model = CastModel
        fields = ('id', 'name')
