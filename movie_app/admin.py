from django.contrib import admin
from .models import MovieModel, CastModel, SliderPic, GenresModel


# admin.site.disable_action('delete_selected')


def make_published(modeladmin, request, queryset):
    queryset.update(published=True)


make_published.short_description = 'انتشار موارد انتخاب شده'


def make_not_published(modeladmin, request, queryset):
    queryset.update(published=False)


make_not_published.short_description = 'عدم انتشار موارد انتخاب شده'


@admin.register(MovieModel)
class MovieModelAdmin(admin.ModelAdmin):
    # fieldsets = (
    #     ('ساده', {
    #         'fields': ('name', 'language')
    #     }),
    #     ('پیشرفته', {
    #         'classes': ('collapse',),
    #         'fields': ('genres', 'view'),
    #     }),
    # )
    search_fields = ('id', 'name',)
    list_display = ('id', 'name', 'thumbnail_tag', 'publisher', 'genres',
                    # 'year',
                    'language', 'published', 'rate', 'view')
    list_display_links = ('id', 'name')
    actions = [make_published, make_not_published]


@admin.register(GenresModel)
class GenresModelAdmin(admin.ModelAdmin):
    readonly_fields = ('level',)
    list_display = ('id', 'genres', 'level')
    list_display_links = ('id', 'genres')


@admin.register(CastModel)
class GenresModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'age')
    list_display_links = ('id', 'name')


admin.site.register(SliderPic)
