from django import template
from ..models import MovieModel, GenresModel
from django.db.models import Prefetch
from django.db import connection, reset_queries

register = template.Library()


@register.inclusion_tag('movie_app/nav.html')
def auto_nav():
    genres = GenresModel.objects.prefetch_related('genresmodel_set').all()

    years = list(MovieModel.objects.
                 values_list('year', flat=True).
                 order_by('-year').
                 distinct())

    languages = list(MovieModel.objects.
                     values_list('language', flat=True).
                     order_by('language').
                     distinct()
                     )

    return {'genres': genres,
            'year': years,
            'languages': languages}


@register.filter
def exclude_parent(full_list, parent):
    return full_list.remove(parent)
