from __future__ import annotations

from django.db.models import Q
from django.shortcuts import get_list_or_404
from django.views.generic import ListView, DetailView, TemplateView, YearArchiveView

from .models import MovieModel, SliderPic


class HomeView(ListView):
    model = MovieModel
    template_name = "movie_app/index.html"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['slider_object_list'] = SliderPic.objects.all()
        return context


class MovieDetailView(DetailView):
    model = MovieModel
    template_name = "movie_app/movie_detail.html"
    query_pk_and_slug = True

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['related_movies'] = MovieModel.objects.exclude(slug=kwargs.get('object').slug)[:6]
        return context


class GenreListView(ListView):
    model = MovieModel
    template_name = "movie_app/genre.html"
    paginate_by = 5

    def get_queryset(self, **kwargs):
        object_list = get_list_or_404(self.model, genres__genres=self.kwargs.get('genre'))
        return object_list

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['genre'] = self.kwargs.get('genre')
        return context


# class YearDateArchiveView(YearArchiveView):
#     queryset = MovieModel.objects.all()
#     date_field = 'year'
#     template_name = 'movie_app/year.html'
#     paginate_by = 5
#     make_object_list = True
#     allow_future = True
#
#     def get_context_data(self, *, object_list=None, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['year'] = self.kwargs.get('year')
#         return context


class YearDateArchiveView(ListView):
    model = MovieModel
    template_name = 'movie_app/year.html'
    paginate_by = 5

    def get_queryset(self, **kwargs):
        object_list = get_list_or_404(self.model, year=self.kwargs.get('year'))
        return object_list


class LanguageListView(ListView):
    model = MovieModel
    template_name = 'movie_app/language.html'
    paginate_by = 5

    def get_queryset(self, **kwargs):
        object_list = get_list_or_404(self.model, language=self.kwargs.get('language'))
        return object_list

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['language'] = self.kwargs.get('language')
        return context


class SearchListView(ListView):
    model = MovieModel
    template_name = 'movie_app/search.html'
    paginate_by = 5

    def get_queryset(self, **kwargs):
        search_char = self.request.GET.get('q')
        if search_char:
            object_list = MovieModel.objects.filter(
                Q(name__icontains=search_char) |
                Q(cast__name__icontains=search_char)
            ).distinct()
        else:
            object_list = self.model.objects.none()
        return object_list
