from django.apps import AppConfig


class MovieAppConfig(AppConfig):
    name = 'movie_app'
    verbose_name = 'مدیریت فیلم'
