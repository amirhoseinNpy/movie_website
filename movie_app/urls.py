from django.urls import path, include
from . import views


app_name = 'movie_app'
# pre-urls = 'movie/'
urlpatterns = [
    path('api/', include('movie_app.api.urls'), name='movie_app_api'),

    path('', views.HomeView.as_view(), name='home'),
    path('mv-<int:pk>/<slug:slug>', views.MovieDetailView.as_view(), name='movie_detail'),
    path('genre/<str:genre>', views.GenreListView.as_view(), name='genre_list'),
    path('year/<int:year>', views.YearDateArchiveView.as_view(), name='year_archive'),
    path('language/<str:language>', views.LanguageListView.as_view(), name='language_list'),
    path('search', views.SearchListView.as_view(), name='search_list'),
]
