from django.test import TestCase
from django.urls import reverse, reverse_lazy
from django.db import connection, reset_queries
from django.db.models import Prefetch

from .models import *


class GenreNavbarModelTest(TestCase):

    def test_2(self):
        for i in ['action', 'drama', 'romance', 'comedy']:
            j = GenresModel.objects.create(genres=i)
            j = GenresModel.objects.create(genres=i+'_L2', genres_type=j)
            j = GenresModel.objects.create(genres=i+'_L3', genres_type=j)
        with self.assertNumQueries(2):
            # q = [GenresModel.objects.select_related('genres_type').all()]
            q = list(GenresModel.objects.prefetch_related(Prefetch('children')).all())
            print(q)
            for i in q:
                print(len(q))
                print(i.genres_type)

            # print(q[1].children)
            # print(q[2].genres_type.genres_type)

    def test_3(self):
        for i in ['action', 'drama', 'romance', 'comedy']:
            j = GenresModel.objects.create(genres=i)
            j = GenresModel.objects.create(genres=i+'_L2', genres_type=j)
            j = GenresModel.objects.create(genres=i+'_L3', genres_type=j)
        with self.assertNumQueries(2):
            genres = list(GenresModel.objects.all()
                          .select_related('genres_type', 'genres_type')
                          )
            print(genres[0].genresmodel_set)
            print(genres[0].genresmodel_set.all())
            print(genres[0].genresmodel_set.all())

    def test_num_of_queries(self):
        _ = GenresModel.objects.create(title='test nav')
        for i in ['action', 'drama', 'romance', 'comedy']:
            j = GenresModel.objects.create(genres=i)
            _.genres_type.add(j)
            j = GenresModel.objects.create(genres=i+'_L1', genres_type=j)
            _.genres_type.add(j)

        # with self.assertNumQueries(1):
        #     reset_queries()
        #     genres = list(MovieModel.objects.values_list('genres', flat=True).order_by("genres").distinct())
            # if len(connection.queries) != 0:
            #     print(connection.queries[0]['time'])
            # else:
            #     print('No query')

    def test_num_of_auto_navbar_queries(self):
        self.assertNumQueries(3, self.client.get, reverse('movie_app:home'))
