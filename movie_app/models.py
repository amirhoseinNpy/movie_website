from datetime import datetime

from django.db import models
from account.models import User
from django_jalali.db import models as jmodels
from django.utils.html import format_html
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
from django.core.exceptions import ValidationError
from django.urls import reverse_lazy


class CastModel(models.Model):
    name = models.CharField(max_length=50, verbose_name='نام')
    age = models.IntegerField(verbose_name='سن')

    class Meta:
        verbose_name = 'بازیگر'
        verbose_name_plural = 'بازیگران'

    def __str__(self):
        return self.name


class MovieModel(models.Model):
    class Language(models.TextChoices):
        ENGLISH = _('english')
        FARSI = _('farsi')

    PUBLISHED_CHOICES = (
        (True, _('منتشر شده')),
        (False, _('منتشر نشده'))
    )

    IS_GOLD_MOVIE_CHOICES = (
        (True, _('طلایی')),
        (False, _('عادی'))
    )

    YEAR_CHOICES = []
    for r in range(2010, datetime.now().year + 1):
        YEAR_CHOICES.append((r, r))

    name = models.CharField(max_length=50, verbose_name='نام')
    description = models.TextField(verbose_name='توضیحات')
    cover = models.ImageField(upload_to='movie_model/images/cover/',
                              verbose_name='تصویر کاور')
    trailer_link = models.CharField(max_length=250, verbose_name='لینک پیش نمایش')
    language = models.CharField(choices=Language.choices, max_length=50,
                                verbose_name='زبان فیلم')
    # year = jmodels.jDateField(verbose_name='تاریخ ساخت فیلم')
    year = models.IntegerField(choices=YEAR_CHOICES,
                               default=datetime.now().year,
                               verbose_name='تاریخ ساخت فیلم')
    genres = models.ForeignKey('GenresModel', on_delete=models.SET_NULL,
                               verbose_name='ژانر', null=True)
    download_link = models.URLField(verbose_name='لینک دانلود')
    watch_link = models.URLField(verbose_name='لینک نمایش')
    view = models.PositiveIntegerField(verbose_name='تعداد بازدید')
    rate = models.DecimalField(
        default=0, validators=[MinValueValidator(0.0), MaxValueValidator(5.0)],
        max_digits=2, decimal_places=1,
        blank=True, null=True, verbose_name='امتیاز'
    )
    slider_pic = models.ImageField(upload_to='movie_model/images/slider_pic/',
                                   null=True, blank=True, verbose_name='تصویر اسلایدر')
    cast = models.ManyToManyField(CastModel, verbose_name='بازیگران')
    slug = models.SlugField(max_length=40, allow_unicode=True, unique=True, blank=True,
                            verbose_name='نامک')
    published = models.BooleanField(choices=PUBLISHED_CHOICES, verbose_name='وضعیت انتشار')
    publisher = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, verbose_name='نویسنده مطلب')
    is_gold_movie = models.BooleanField(default=False, choices=IS_GOLD_MOVIE_CHOICES, verbose_name='وضعیت دسترسی طلایی')

    class Meta:
        verbose_name = 'فیلم'
        verbose_name_plural = 'فیلم ها'

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:  # blank slug input
            self.slug = slugify(self.name, allow_unicode=True)
        else:
            self.slug = slugify(self.slug, allow_unicode=True)

        return super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse_lazy('account:home')

    def thumbnail_tag(self):
        return format_html('<img width=75 height=100 style="border-radius: 10px;" src="{}">'.format(self.cover.url))

    thumbnail_tag.short_description = 'پیشنمایش کاور'


class SliderPic(models.Model):
    movie_model = models.ForeignKey('MovieModel', on_delete=models.CASCADE,
                                    verbose_name='انتخاب فیلم')

    class Meta:
        verbose_name = 'اسلایدر'
        verbose_name_plural = 'اسلایدرها'

    def clean(self):
        if not self.movie_model.slider_pic:
            raise ValidationError('slider_pic of "{}" is empty'.format(self.movie_model))

    def __str__(self):
        return self.movie_model.name + ' Slider'


class GenresModel(models.Model):
    genres_type = models.ForeignKey('self', on_delete=models.SET_NULL,
                                    null=True, blank=True,
                                    verbose_name='زیر منوی ژانر')
    genres = models.CharField(max_length=50, verbose_name='ژانر')
    level = models.PositiveIntegerField(verbose_name='سطح', blank=True)

    class Meta:
        verbose_name = 'ژانر'
        verbose_name_plural = 'ژانرها'

    def __str__(self):
        return self.genres

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.genres_type:
            self.level = self.genres_type.level + 1
        else:
            self.level = 1

        return super().save()
