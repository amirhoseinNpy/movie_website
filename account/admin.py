from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User


UserAdmin.fieldsets += ('فیلد های خاص من',
                        {'fields': ('is_author', 'gold_user')}),
UserAdmin.list_display = ('username', 'email', 'first_name',
                          'last_name', 'is_staff', 'is_gold_user')

admin.site.register(User, UserAdmin)
