from django.contrib.auth import views as user_view
from django.contrib.auth.decorators import login_required
from django.urls import path, include
from . import views

app_name = 'account'
# pre-urls = 'account/'
urlpatterns = [
    path('login/', user_view.LoginView.as_view(), name='login'),
    path('logout/', user_view.LogoutView.as_view(), name='logout'),
    #
    # path('password_change/', views.PasswordChangeView.as_view(), name='password_change'),
    # path('password_change/done/', views.PasswordChangeDoneView.as_view(), name='password_change_done'),
    #
    # path('password_reset/', views.PasswordResetView.as_view(), name='password_reset'),
    # path('password_reset/done/', views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    # path('reset/<uidb64>/<token>/', views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    # path('reset/done/', views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
]

urlpatterns += [
    path('api/', include('account.api.urls'), name='account_api'),
    path('home/', views.HomeView.as_view(), name='home'),
    path('create/', views.MovieCreateView.as_view(), name='create'),
    path('update/<int:pk>/', views.MovieUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', views.MovieDeleteView.as_view(), name='delete'),
    path('profile/', views.UserProfileView.as_view(), name='profile'),
]
