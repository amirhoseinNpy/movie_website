from django.http import Http404
from django.shortcuts import get_object_or_404

from movie_app.models import MovieModel


class FieldsMyMixins:

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser:
            self.fields = ('name', 'language', 'year', 'genres',
                           'description', 'cover', 'trailer_link',
                           'download_link', 'watch_link', 'slider_pic',
                           'view', 'rate', 'cast', 'publisher', 'published', 'is_gold_movie')
        elif request.user.is_author:
            self.fields = ('name', 'language', 'year', 'genres',
                           'description', 'cover', 'trailer_link',
                           'download_link', 'watch_link', 'slider_pic',
                           'view', 'rate', 'cast', 'is_gold_movie')
        else:
            raise Http404('داری اشتباه میزنی!')

        return super().dispatch(request, *args, **kwargs)


class ValidationMyMixins:

    def form_valid(self, form):
        if self.request.user.is_superuser:
            form.save()

        elif self.request.user.is_author:
            form2 = form.save(commit=False)
            form2.publisher = self.request.user
            form2.published = True

        return super().form_valid(form)


class PublisherAccessMyMixins:

    def dispatch(self, request, pk, *args, **kwargs):
        movie_object = get_object_or_404(MovieModel, pk=pk)
        if movie_object.publisher == request.user and not movie_object.published or request.user.is_superuser:

            return super().dispatch(request, *args, **kwargs)

        else:
            raise Http404('Access is Denied')


class SuperUserAccessMyMixins:

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        else:
            raise Http404('Access is Denied.you are not SuperUser')
