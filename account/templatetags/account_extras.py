from django import template


register = template.Library()


@register.simple_tag
def row_num(row_number, page_number, per_page):
    return row_number + (per_page*(page_number-1))


@register.inclusion_tag('account/active_link.html')
def active_link(request, reverse_link, content, klass):
    return {
        'request': request,
        'reverse_link': reverse_link,
        'content': content,
        'klass': klass,
    }
