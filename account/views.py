from django.views.generic import (ListView,
                                  CreateView,
                                  UpdateView,
                                  DeleteView)
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from movie_app.models import MovieModel
from .models import User
from .forms import UserProfileForm
from .mixins import (FieldsMyMixins,
                     ValidationMyMixins,
                     PublisherAccessMyMixins,
                     SuperUserAccessMyMixins)


class HomeView(LoginRequiredMixin, ListView):
    model = MovieModel
    template_name = 'account/home.html'
    paginate_by = 3

    def get_queryset(self):
        if self.request.user.is_superuser:
            return self.model.objects.all()
        else:
            return self.model.objects.filter(publisher=self.request.user)


class MovieCreateView(LoginRequiredMixin, FieldsMyMixins,
                      ValidationMyMixins, CreateView):
    model = MovieModel
    template_name = 'account/movie_create_update.html'


class MovieUpdateView(LoginRequiredMixin, FieldsMyMixins,
                      ValidationMyMixins, PublisherAccessMyMixins, UpdateView):
    model = MovieModel
    template_name = 'account/movie_create_update.html'


class MovieDeleteView(LoginRequiredMixin, SuperUserAccessMyMixins,
                      DeleteView):
    model = MovieModel
    success_url = reverse_lazy('account:home')
    template_name = 'account/movie_delete_confirm.html'


class UserProfileView(LoginRequiredMixin, UpdateView):
    model = User
    form_class = UserProfileForm
    success_url = reverse_lazy('account:profile')
    template_name = 'account/user_profile_update.html'

    def get_object(self, queryset=None):
        return self.model.objects.get(pk=self.request.user.pk)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        # kwargs['user'] = self.request.user
        kwargs.update({
            'user': self.request.user
        })
        return kwargs
