from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    is_author = models.BooleanField(default=False,
                                    verbose_name='وضعیت نویسندگی')
    gold_user = models.DateTimeField(default=timezone.now,
                                     verbose_name='کاربر طلایی تا')

    def is_gold_user(self):
        if self.gold_user > timezone.now():
            return True
        return False

    is_gold_user.short_description = 'وضعیت طلایی'
    is_gold_user.boolean = True
