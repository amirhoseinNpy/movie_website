from django import forms

from .models import User


class UserProfileForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super().__init__(*args, **kwargs)
        if not user.is_superuser:
            self.fields['username'].disabled = True
            self.fields['is_author'].disabled = True
            self.fields['gold_user'].disabled = True

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name',
                  'email', 'is_author', 'gold_user')
