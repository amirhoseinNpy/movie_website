from django.urls import path
from django.contrib.auth.models import User

from . import views


# pre-urls = 'account/api/'
urlpatterns = [
    path('users/', views.UserCreateApiView.as_view(), name='create_user'),
    path('login/', views.LoginApiView.as_view(), name='login_api'),
]
