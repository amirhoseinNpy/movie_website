from rest_framework import generics, status
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from account.models import User
from django.contrib.auth import authenticate

from .serializers import UserSerializer


class LoginApiView(APIView):
    permission_classes = ()

    def post(self, request):
        username = request.data.get('username')
        password = request.data.get('password')
        user = authenticate(username=username, password=password)
        if user:
            try:
                return Response({'token': user.auth_token.key})
            except User.auth_token.RelatedObjectDoesNotExist:
                Token.objects.create(user=user)
                return Response({'token': user.auth_token.key})
        else:
            return Response({'error': 'Wrong Credentials'},
                            status=status.HTTP_400_BAD_REQUEST)


class UserCreateApiView(generics.CreateAPIView):
    # authentication_classes = ()
    # permission_classes = ()
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get(self, request):
        print(request.user)
        return Response({'error': 'Get method not available'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)

